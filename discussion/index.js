/*
console.log(varSample);
// undefined but not error
var varSample = "Hoist me up"; 
*/

// Exponent Operator

// Before ES6
// Math.pow(base, exponent)
let fivePowerOf3 = Math.pow(5, 3); 
console.log(fivePowerOf3);

// Exponent Operators ES6
// Syntax: ** - get result of a number raised to a given exponent

let fivePowerOf2 = 5 ** 2; // base & exponent
console.log(fivePowerOf2); // 25

let fivePowerOf4 = 5 ** 4;
console.log(fivePowerOf4); // 625

let squareRootOf4 = 4 ** .5; // .5 is for sqaure root
console.log(squareRootOf4);

let string1 = "Javascript";
let string2 = "not";
let string3 = "is";
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";

// let sentence1, sentence2;
// sentence1 = string1 + " " + string3 + " " + string2 + " " + string5;
// sentence2 = string4 + " " + string3 + " " + string1;
// console.log(sentence1);
// console.log(sentence2);

// Concatenating String ES6

// Template Literals allows to create strings using `` and emebed expression in it

/*
    ${} is a placeholder it is used to embed JS expressions
*/

let sentence1 = `${string1} ${string3} ${string2} ${string5}`;
let sentence2 = `${string4} ${string3} ${string1}`;
console.log(sentence1);
console.log(sentence2);

let sentence3 = `${string6} ${string7} Bootcamp`;
console.log(sentence3);

let sentence4 = `The sum of 15 and 25 is ${15 + 25}`;
console.log(sentence4);

let person = {
    name: "Michael",
    position: "developer",
    income: 50000,
    expenses: 60000
}

console.log(`${person.name} is a ${person.position}`);
console.log(`His income is ${person.income} and expeses at ${person.expenses}. His current balance ${person.income - person.expenses}`);

// Destructuring Array and Objects
// allows to save array items or object properties into new variables without having to create/initialize with accessing the items/properties one by one

let array1 = ["Curry", "Lillard", "Paul", "Irving"];
/*
let player1 = array1[0];
let player2 = array1[1];
let player3 = array1[2];
let player4 = array1[3];

console.log(player1, player2, player3, player4);
*/

// Array destructuring is when we save array items into variables
// Order matters in normal array and destructuring
let [player1, player2, player3, player4] = array1;
console.log(player1, player2, player3, player4);

let array2 = ["Jokic", "Embiid", "Howard", "Anthony-Towns"];

// get and save all items into variables except for Howard
let [center1, center2, , center4] = array2; // skipping Howard
console.log(center4); // result Anthony-Towns

// Object Destructuring
// Orders doesn't matter however name of var must match property in the object

let pokemon1 = {
    name: "Bulbasaur",
    type: "Grass",
    level: 10,
    moves: ["Razor Leaf", "Tackle", "Leech Seed"]
}

let {level, type, name, moves, personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality); // undef not found in obj

let pokemon2 = {
    name: "Charmander",
    type: "Fire",
    level: 11,
    moves: ["Ember", "Scratch"]
}

// {propertyName: newVariable}
const {name: name2} = pokemon2;
console.log(name2);

/*
// let {moves, name, type, level} = pokemon2; // error
let {name2 = name} = pokemon2; // working with new variable
console.log(name2);
*/

// Arrow Functions
// Alternative way with pros and cons between traditional and arrow

// Traditional

function displayMsg() {
    console.log("Hello World!");
}
displayMsg();

// Arrow Function
const hello = () => { /* parameter insdie () */
    console.log(`Hello from Arrow Function!`);
}

hello();

// Arrow Function with Parameters

const greet = (friend) => {
    // console.log(friend);
    console.log(`Hi! ${friend.name}`); 
    console.log(`Hi! ${friend.position}`); 
} 

greet(person);

// Arrow vs Traditional
// Implicit Return - return value from arrowFunc w/o return keyword
/*

function addNum(num1, num2) {
    return num1 + num2;
}
console.log(addNum(5, 10));

const subNum = (num1, num2) => num1 - num2; // no {} one liner code
console.log(subNum(10, 5));
*/
// Implicit return will only work on arrow function w/o {}
// {} in arrow functions are code blocks
// If ann arrow function has a {}, we're going to need use return

/* Multi line of code nees {} and return keyword */

const subNum2 = (num1, num2) => {return num1 - num2};
console.log(subNum2(20, 5));

/* Mini */

const addNum = (num1, num2) => num1 + num2;
let sum = addNum(50, 70);
console.log(sum);

let character1 = {
    name: "Cloud Strife",
    occupation: "SOLDIER",
    greet: function() {
        // this refers to current object
        console.log(this);
        console.log(`Hi! I'm ${this.name}`);
    },
    introduceJob: () => {

        // this keyword will not refer to parent object
        // this refers to global window object inside arrow function
        // console.log(this); // this returns window...
    }
}
character1.greet();
character1.introduceJob();

// Class based object
/*
    In JS - classes are templates of objects
    We can create objects out of the use of classes
    Constructor function was used to to create objects
*/

// Constructor function

function pokemon(name, type, level) {
    this.name = name;
    this.type = type;
    this.level = level;
}

// ES6 - there's special method of creating and initializing object

class Car {
    constructor(brand, name, year) {
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}

let car1 = new Car("Toyota", "Vios", "2002");
let car2 = new Car("Cooper", "Mini", "1969");
let car3 = new Car("Porshe", "911", "1967");

console.log(car1);
console.log(car2);
console.log(car3);

/* Mini Activity */

class PokemonDetails{
    constructor(name, type, level) {
        this.name = name;
        this.type = type;
        this.level = level;
    }
}

let pokemonDetail1 = new PokemonDetails("Wapapet", "Bear", 15);
let pokemonDetail2 = new PokemonDetails("Meow", "Cat", 17);

console.log(pokemonDetail1);
console.log(pokemonDetail2);