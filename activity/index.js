/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.

	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.

	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

/*Debug and Update*/

function introduce(student){

	// console.log("Hi! I'm " + student.names + "." + "I am " + student.ages + " years old.")
	//console.log("I study the following courses: " + student.class)
    console.log(`Hi I'm ${student.name}. I am ${student.age} years old.`);
    console.log(`I study the following courses: ${student.classes} `);
}

introduce(student1);
introduce(student2);

// function getCube(num){
// 	console.log(Math.pow(num,3));
// }

console.log("Display cube of a number:");
const getCube = (num) => console.log(num ** 3);
getCube(5);

let numArr = [15,16,32,21,21,2];

// numArr.forEach(function(num){
// 	console.log(num);
// });


console.log("Displaying forEach in => Function ");

// single line
numArr.forEach((num) => console.log(num));

/*
Multi-line
numArr.forEach((num) => {
    console.log(num);
});
*/


// let numsSquared = numArr.map(function(num){
// 	return number ** 2;
//   }
// )
// console.log(numsSquared + "squred");

// const numsSquared = numArr.map(num) => num ** 2;

console.log("Display square of a number:");

// single line
const numsSquared = numArr.map((num) => num ** 2);
console.log(numsSquared);

/*
Multi-line
const numsSquared = numArr.map(num => {
    num = num ** 2;
    return num;
});
*/

/*2. Class Constructor*/

class Player {
    constructor(username, role, guildName, level) {
        this.username = username;
        this.role = role;
        this.guildName = guildName;
        this.level = level;
    }
}

let player1 = new Player("sotoruberi", "wizard", "CoolBreezer", 55);
let player2 = new Player("Jineyo", "barbarian", "LupangPinako", 75);

console.log("Class Player");
console.log(player1);
console.log(player2);